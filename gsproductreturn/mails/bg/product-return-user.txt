
[/{shop_url}] 

Здравейте {username},

Тук са вашите данните за връщане:

E-MAIL АДРЕС : {email}

ВАШЕТО ИМЕ: {username}

ТЕЛЕФОНЕН НОМЕР : {userPhoneNumber}

ДАТА НА ЗАЯВКАТА: {dateOfRequest}

ПРИЧИНА ЗА ВРЪЩАНЕ: {reasonForReturn}

ТИП НА ВРЪЩАНЕТО: {typeOfReturn}

ОПИСАНИЕ НА ПРОДУКТА ЗА КОЙТО ИСКАТЕ ЗАМЯНА(АКО ИМА ТАКЪВ!) : {productDescription}


Сега можете да правите поръчки в
нашия магазин: {shop_name} [/{shop_url}] 

{shop_name} [/{shop_url}] се управлява с
PrestaShop(tm) [http://www.prestashop.com/] 

