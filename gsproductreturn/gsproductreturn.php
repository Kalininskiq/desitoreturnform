<?php
/**
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Gsproductreturn extends Module
{
    protected $config_form = false;

    public function __construct()
    {
        $this->name = 'gsproductreturn';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Kalin Ivanov';
        $this->need_instance = 1;
        $this->controllers = array('return');
        /**
         * Set $this->bootstrap to true if your module is compliant with bootstrap (PrestaShop 1.6)
         */
        $this->bootstrap = true;
        parent::__construct();
        $this->displayName = $this->l('Product Return Module');
        $this->description = $this->l('Generate a page for product returning');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall the module?');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }

    /**
     * Don't forget to create update methods if needed:
     * http://doc.prestashop.com/display/PS16/Enabling+the+Auto-Update
     */
    public function install()
    {
        Configuration::updateValue('GSPRODUCTRETURN_LIVE_MODE', false);
        include(dirname(__FILE__).'/sql/install.php');

        return (
            parent::install() &&
            $this->installTab()&&
            $this->registerHook('header') &&
            $this->registerHook('backOfficeHeader') &&
            $this->registerHook('displayLeftColumn')
        );
    }

    public function uninstall()
    {
        Configuration::deleteByName('GSPRODUCTRETURN_ACCOUNT_EMAIL');
        Configuration::deleteByName('GSPRODUCTRETURN_CMS_PAGE_DATA_POLICY');
        Configuration::deleteByName('GSPRODUCTRETURN_CMS_PAGE_TERMS');

        include(dirname(__FILE__).'/sql/uninstall.php');

        return parent::uninstall()
            && $this->uninstallTab();
    }

    public function installTab()
    {
        $tab = new Tab();
        $tab->active = 1;
        $tab->class_name = 'AdminProductReturn';
        $tab->name = array();
        $tab->route_name = 'admin_my_symfony_routing';
        foreach (Language::getLanguages(true) as $lang) {
            $tab->name[$lang['id_lang']] = 'Product Return';
        }
        if (version_compare(_PS_VERSION_, '1.7.0.0', '>=')) {
            //AdminPreferences
            $tab->id_parent = (int)Db::getInstance(_PS_USE_SQL_SLAVE_)
                ->getValue(
                    'SELECT MIN(id_tab)
                        FROM `'._DB_PREFIX_.'tab`
                        WHERE `class_name` = "'.pSQL('ShopParameters').'"'
                );
        } else {
            // AdminAdmin
            $tab->id_parent = (int)Tab::getIdFromClassName('AdminOrders');
        }
        $tab->module = $this->name;
        return $tab->save();
    }

    private function uninstallTab()
    {
        $tabId = (int) Tab::getIdFromClassName('AdminProductReturn');
        if (!$tabId) {
            return true;
        }
        $tab = new Tab($tabId);
        return $tab->delete();
    }

    /**
     * Load the configuration form
     */
    public function getContent()
    {
        /**
         * If values have been submitted in the form, process.
         */
        if (((bool)Tools::isSubmit('submitGsproductreturnModule')) == true) {
            $this->postProcess();
        }

        $this->context->smarty->assign('module_dir', $this->_path);

        $output = $this->context->smarty->fetch($this->local_path.'views/templates/admin/configure.tpl');

        return $output.$this->renderForm();
    }

    /**
     * Create the form that will be displayed in the configuration of your module.
     */
    protected function renderForm()
    {
        $helper = new HelperForm();

        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $helper->module = $this;
        $helper->default_form_language = $this->context->language->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitGsproductreturnModule';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFormValues(), /* Add values for your inputs */
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
        );

        return $helper->generateForm(array($this->getConfigForm()));
    }

    /**
     * Create the structure of your form.
     */
    protected function getConfigForm()
    {
        $cmsPages = CMS::getCMSPages($this->context->language->id);

        return array(
            'form' => array(
                'legend' => array(
                'title' => $this->l('Settings'),
                'icon' => 'icon-cogs',
                ),
                'input' => array(
                    array(
                        'col' => 3,
                        'type' => 'text',
                        'prefix' => '<i class="icon icon-envelope"></i>',
                        'desc' => $this->l('Enter a valid email address'),
                        'name' => 'GSPRODUCTRETURN_ACCOUNT_EMAIL',
                        'label' => $this->l('Email'),
                    ),
                    array(
                        'type' =>  'select',
                        'label' => $this->l('Select a CMS page for the data policy page'),
                        'name' => 'GSPRODUCTRETURN_CMS_PAGE_DATA_POLICY',
                        'required' => true,
                        'options' => array(
                            'query' => $cmsPages,
                            'id' => 'id_cms',
                            'name' => 'meta_title',
                        )
                    ),
                    array(
                        'type' =>  'select',
                        'label' => $this->l('Select a CMS page for the general terms and conditions page'),
                        'name' => 'GSPRODUCTRETURN_CMS_PAGE_TERMS',
                        'required' => true,
                        'options' => array(
                            'query' => $cmsPages,
                            'id' => 'id_cms',
                            'name' => 'meta_title',
                        )
                    ),
                ),

                'submit' => array(
                    'title' => $this->l('Save'),
                ),
            ),
        );
    }

    /**
     * Set values for the inputs.
     */
    protected function getConfigFormValues()
    {
        return array(
            'GSPRODUCTRETURN_ACCOUNT_EMAIL' => Tools::getValue('GSPRODUCTRETURN_ACCOUNT_EMAIL'),
            'GSPRODUCTRETURN_CMS_PAGE_DATA_POLICY'=>
                Tools::getValue('GSPRODUCTRETURN_CMS_PAGE_DATA_POLICY'),
            'GSPRODUCTRETURN_CMS_PAGE_TERMS'=>
                Tools::getValue('GSPRODUCTRETURN_CMS_PAGE_TERMS'),
        );
    }

    /**
     * Save form data.
     */
    protected function postProcess()
    {
        $form_values = $this->getConfigFormValues();

        foreach (array_keys($form_values) as $key) {
            Configuration::updateValue($key, Tools::getValue($key));
        }
    }

    /**
    * Add the CSS & JavaScript files you want to be loaded in the BO.
    */
    public function hookBackOfficeHeader()
    {
        if (Tools::getValue('module_name') == $this->name) {
            $this->context->controller->addJS($this->_path.'views/js/back.js');
            $this->context->controller->addCSS($this->_path.'views/css/back.css');
        }
    }

    /**
     * Add the CSS & JavaScript files you want to be added on the FO.
     */
    public function hookHeader()
    {

        $urlDataPolicyCMS = $this->context->link->getCMSLink(Configuration::get("GSPRODUCTRETURN_CMS_PAGE_DATA_POLICY"));
        $urlGeneralTerms = $this->context->link->getCMSLink(Configuration::get("GSPRODUCTRETURN_CMS_PAGE_TERMS"));

        Media::addJsDef([
            'UrlToController' => Context::getContext()->link->
            getModuleLink('gsproductreturn', 'return', array('ajax'=>true)),
            'UrlToDataPolicy' => $urlDataPolicyCMS,
            'UrlToGeneralTerms' => $urlGeneralTerms,
        ]);
        $this->context->controller->addJS($this->_path.'/views/js/returnRequest.js');
        $this->context->controller->addJS($this->_path.'/views/js/front.js');
        $this->context->controller->addCSS($this->_path.'/views/css/front.css');
    }

    public function hookDisplayLeftColumn()
    {
        $this->context->smarty->assign(
            array(
                'frontController_link' => $this->context->link->getModuleLink('gsproductreturn', 'return')
            )
        );

        return $this->display(__FILE__, "link.tpl");
    }
}
