<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

class ProductReturnRequest extends ObjectModel
{
    public $userName;
    public $userEmail;
    public $dateOfRequest;
    public $typeOfReturn;
    public $userPhoneNumber;
    public $reasonForReturn;
    public $productDescription;
    public $productStatus;

    public static $definition = array(
        'table' => 'gsproductreturn',
        'primary' => 'id_gsproductreturn',
        'multilang' => false,
        'fields' => array(
            'dateOfRequest' => array('type' => self::TYPE_DATE,'valudate' => 'isDate'),
            'userName' =>        array('type' => self::TYPE_STRING,'validate' => 'isName'),
            'userEmail' =>        array('type' => self::TYPE_STRING,'validate' => 'isEmail'),
            'userPhoneNumber' =>        array('type' => self::TYPE_STRING,'validate' => 'isPhoneNumber'),
            'reasonForReturn' => array('type' => self::TYPE_STRING,'validate' => 'isCleanHTML'),
            'typeOfReturn' => array('type' => self::TYPE_STRING,'validate' => 'isCleanHTML'),
            'productDescription' => array('type' => self::TYPE_STRING,'validate '=> 'isCleanHTML'),
            'productStatus' => array('type' => self::TYPE_INT)
        ),
    );
}
