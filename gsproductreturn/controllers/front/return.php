<?php
/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */

require_once('modules/gsproductreturn/classes/ProductReturnRequest.php');
class GsproductreturnReturnModuleFrontController extends ModuleFrontController
{
    private $productReturn ;
    public function __construct()
    {
        parent::__construct();
        $this->display_column_left = false;
        $this->display_column_right = false;
    }

    public function initContent()
    {
        parent::initContent();
        $errors =  [];
        $message = [];

        if (Tools::getValue("ajax") == 1) {
            if (!Validate::isEmail(Tools::getValue("user_email")) || empty($_POST['user_email'])) {
                $errors['email'] = $this->module->l('Email field is empty or incorrect.', 'return');
            }
            if (empty(Tools::getValue('return_type'))) {
                $errors["return-type"] =  $this->module->l('Please choose a type of your return.', 'return');
            }
            if (empty(Tools::getValue('user_name'))) {
                $errors["username"] = $this->module->l('Empty username field.', 'return');
            }
            if (!Validate::isPhoneNumber(Tools::getValue('user_phone')) ||
                empty(Tools::getValue('user_phone'))) {
                $errors["phone"] = $this->module->l('Phone number field empty or incorrect.', 'return');
            }
            if (empty(Tools::getValue('return_reason'))) {
                $errors["return-reason"] = $this->module->l('Please choose a reason for your return.', 'return');
            }
            if (Tools::getValue('data_policy') == "false" || Tools::getValue('terms') == "false") {
                $errors["checkboxes"] = $this->module->l('Please agree to the  data policy and terms and conditions for product returns.', 'return');
            }
            if (Tools::getValue('return_type') == "Replacement for another product") {
                $var = trim(Tools::getValue('product_description'));
                if (isset($var) === true && $var === '') {
                    $errors["description"] = $this->module->l('Please fill in the product description field.', 'return');
                }
            }
            if (!empty($errors)) {
                die(json_encode(['errors' => $errors]));
            } else {
                $this->sendToDb();
                $message[] = $this->module->l('Request successfully made', 'return');
                $this->sendEmailUser();
                $this->sendEmailAdmin();
                die(json_encode(['success' => $message]));
            }
        }
        $this->setTemplate('returnForm.tpl');
    }

    public function sendToDb()
    {
        $this->productReturn = new ProductReturnRequest();
        $this->productReturn->userEmail = Tools::getValue('user_email');
        $this->productReturn->dateOfRequest = date("Y-m-d H:i:s");
        $this->productReturn->userName = Tools::getValue('user_name');
        $this->productReturn->reasonForReturn = Tools::getValue('return_reason');
        $this->productReturn->typeOfReturn = Tools::getValue('return_type');
        $this->productReturn->userPhoneNumber = Tools::getValue('user_phone');
        $this->productReturn->productDescription = Tools::getValue('product_description');
        $this->productReturn->productStatus  = 2;
        $this->productReturn->add();
    }
    public function sendEmailUser()
    {
        Mail::Send(
            (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
            'product-return-user', // email template file to be use
            Mail::l('Product Return'), // email subject
            array(
                '{email}' => $this->productReturn->userEmail, // sender email address
                '{message}' => Mail::l('You have successfully made a return request'),// email content
                '{username}' => $this->productReturn->userName,
                '{dateOfRequest}' => $this->productReturn->dateOfRequest,
                '{reasonForReturn}' => $this->productReturn->reasonForReturn,
                '{typeOfReturn}' =>  $this->productReturn->typeOfReturn,
                '{userPhoneNumber}' => $this->productReturn->userPhoneNumber,
                '{productDescription}' => $this->productReturn->productDescription,
            ),
            $this->productReturn->userEmail, // receiver email address
            "", //to name
            null, //from email address
            "Presta", //from name
            null, // file attachment
            null, //mode_smtp
            _PS_MODULE_DIR_.'gsproductreturn/mails/', // template path
            null, // die
            null, //id shop
            null, // null
            null // reply to
        );
    }

    public function sendEmailAdmin()
    {
        Mail::Send(
            (int)(Configuration::get('PS_LANG_DEFAULT')), // defaut language id
            'admin', // email template file to be use
            Mail::l('Product Return'), // email subject
            array(
                '{email}' => $this->productReturn->userEmail, // sender email address
                '{message}' => Mail::l('You have successfully made a return request'),// email content
                '{username}' => $this->productReturn->userName,
                '{dateOfRequest}' => $this->productReturn->dateOfRequest,
                '{reasonForReturn}' => $this->productReturn->reasonForReturn,
                '{typeOfReturn}' =>  $this->productReturn->typeOfReturn,
                '{userPhoneNumber}' => $this->productReturn->userPhoneNumber,
                '{productDescription}' => $this->productReturn->productDescription,
            ),
            Configuration::get("GSPRODUCTRETURN_ACCOUNT_EMAIL"), // receiver email address
            "", //to name
            null, //from email address
            "Presta", //from name
            null, // file attachment
            null, //mode_smtp
            _PS_MODULE_DIR_.'gsproductreturn/mails/', // template path
            null, // die
            null, //id shop
            null, // null
            null // reply to
        );
    }
}
