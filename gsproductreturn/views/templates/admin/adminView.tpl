{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{*<div >*}
{*    <h1>{l s="Information about the product for return" d="gsproductreturn"}</h1>*}
{*    <table class="table  table-dark">*}
{*        <thead class="bg-primary">*}
{*        <tr>*}
{*            <th scope="col"><b>Request ID</b></th>*}
{*            <th scope="col"><b>Username</b></th>*}
{*            <th scope="col"><b>User email</b></th>*}
{*            <th scope="col"><b>Date of the request</b></th>*}
{*            <th scope="col"><b>Product Description</b></th>*}
{*            <th scope="col"><b>Return Type</b></th>*}
{*            <th scope="col"><b>Return Reason</b></th>*}
{*        </tr>*}
{*        </thead>*}
{*        <tbody >*}
{*        <tr class="bg-primary">*}
{*            <td>{$id|escape:'htmlall':'UTF-8'}</td>*}
{*            <td>{$userName|escape:'htmlall':'UTF-8'}</td>*}
{*            <td>{$userEmail|escape:'htmlall':'UTF-8'}</td>*}
{*            <td>{$dateOfRequest|escape:'htmlall':'UTF-8'}</td>*}
{*            <td>{$userPhoneNumber|escape:'htmlall':'UTF-8'}</td>*}
{*            <td>{$reasonForReturn|escape:'htmlall':'UTF-8'}</td>*}
{*            <td>{$typeOfReturn|escape:'htmlall':'UTF-8'}</td>*}
{*            <td>{$productDescription|escape:'htmlall':'UTF-8'}</td>*}
{*        </tr>*}
{*        </tbody>*}
{*    </table>*}

{*</div>*}
