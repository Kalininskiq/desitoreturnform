{*
* 2007-2020 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2020 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
<form id="productReturnForm" method="POST">
    <h1>{l s='Product Return Form' mod='gsproductreturn'}</h1>
    <hr>
{*RETURN TYPES*}
    <span id="return-type-error">

    </span>
    <div class="form-check" id="returnType">
        <label class="form-check-label" for="returnReason">{l s='Type of replacement:' mod='gsproductreturn'}</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="returnType" id="coupon" value="{l s='Issues a voucher' mod='gsproductreturn'}">
            <label class="form-check-label" for="coupon">
               {l s='Issue a voucher for the price of the product' mod='gsproductreturn'}
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="returnType" id="returnProduct" value="{l s='Replacement for another product' mod='gsproductreturn'}">
            <label class="form-check-label" for="returnProduct">
                {l s='Replacement for another product' mod='gsproductreturn'}
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="returnType" id="returnCash" value="{l s='Return cash' mod='gsproductreturn'}">
            <label class="form-check-label" for="returnCash">
                {l s='Return the amount of money' mod='gsproductreturn'}
            </label>
        </div>
    </div>


{* Input fields section *}
    <span id="product-description-error">

    </span>
    <div id="product_description">
        <div class="form-label">
            <label class="form-check-label" for="productDescription">{l s='Only for replacement - describe the product, you want:' mod='gsproductreturn'}</label>
        </div>
        <div class="form-group">
        <textarea name="message" id="productDescription" class="form-control">
        </textarea>
        </div>
    </div>

<div id="user-box">
    <span id="username-error">

    </span>
    <div class="form-group">
        <label for="userName" id="username-label"><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-person" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M10 5a2 2 0 1 1-4 0 2 2 0 0 1 4 0zM8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6zm6 5c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4zm-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10c-2.29 0-3.516.68-4.168 1.332-.678.678-.83 1.418-.832 1.664h10z"/>
            </svg> {l s='Your name' mod='gsproductreturn'}</label>
        <input type="text" name="user_name" id="userName" class="form-control"  >
    </div>
    <span id="email-error">

    </span>
    <div class="form-group">

        <label for="userEmail" id="email-label"><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-envelope" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z"/>
            </svg>  {l s='E-mail (IMPORTANT! Specify a correct, correctly written e-mail for correspondence):' mod='gsproductreturn'}</label>
        <input type="email" name="email" id="userEmail" class="form-control"  >
    </div>
    <span id="phone-error">

    </span>
    <div class="form-group">

        <label for="userPhone" id="phone-label"><svg width="1.5em" height="1.5em" viewBox="0 0 16 16" class="bi bi-phone" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M11 1H5a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1zM5 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h6a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2H5z"/>
                <path fill-rule="evenodd" d="M8 14a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
            </svg> {l s='Phone for contact' mod='gsproductreturn'}</label>
        <input type="email" name="userPhone" id="userPhone" class="form-control" >
    </div>
</div>


{* Return Reasons *}
    <div id="returnReasons">
        <div class="form-check">
            <label class="form-check-label" for="returnReason">{l s='What is the reason you want to return this item? :' mod='gsproductreturn'}</label>
        </div>
        <div class="form-check">
            <input  type="radio" name="returnReason" id="tooBig" value="Too big" checked>
            <label class="form-check-label" for="tooBig">
                {l s='Too big' mod='gsproductreturn'}
            </label>
        </div>
        <div class="form-check">
            <input  type="radio" name="returnReason" id="tooSmall" value="Too small">
            <label class="form-check-label" for="tooSmall">
                {l s='Too small' mod='gsproductreturn'}
            </label>
        </div>
        <div class="form-check">
            <input  type="radio" name="returnReason" id="wrongProduct" value="Wrong Item">
            <label class="form-check-label" for="wrongProduct">
                {l s='Wrong item' mod='gsproductreturn'}
            </label>
        </div>
        <div class="form-check">
            <input  type="radio" name="returnReason" id="differentFromPicture" value="Different From Pictures">
            <label class="form-check-label" for="differentFromPicture">
                {l s='Different from the pictures' mod='gsproductreturn'}
            </label>
        </div>
        <div class="form-check">
            <input  type="radio" name="returnReason" id="productQuality" value="Quality of the item">
            <label class="form-check-label" for="productQuality">
                {l s='Quality of the item' mod='gsproductreturn'}
            </label>
        </div>
        <div class="form-check">
            <input  type="radio" name="returnReason" id="defectiveProduct" value="Defective Product">
            <label class="form-check-label" for="defectiveProduct">
                {l s='Defective item' mod='gsproductreturn'}
            </label>
        </div>
        <div class="form-check">
            <input  type="radio" name="returnReason" id="dontLookGood" value="Doesnt look good on me">
            <label class="form-check-label" for="dontLookGood">
                {l s='Doesnt look good on me' mod='gsproductreturn'}
            </label>
        </div>
        <div class="form-check">
            <input type="radio" name="returnReason" id="slowDelivery" value="Slow delivery" >
            <label class="form-check-label" for="slowDelivery">
                {l s='Delayed delivery' mod='gsproductreturn'}
            </label>
        </div>
    </div>

    {* CHECK BOXES *}
        <span id="checkbox-error">

        </span>
        <div id="checkboxes">
            <div class="form-group">
                <label class="form-check-label" for="termsAndConditions">
                    {l s='I have read and accept the general terms and conditions for replacement and the personal data policy (mandatory):'|unescape  mod='gsproductreturn'}
                </label>
                </br>

                <input class="form-check-input" type="checkbox" name="termsAndConditions" id="termsAndConditions" >
            </div>
        </div>

    <button type="submit"  id="ReturnProductButton">{l s='Submit' mod='gsproductreturn'}</button>

    <br>
    <div id="success">

    </div>

    <div id="linksToCMSPages">
        <a id="linktoTermsAndContidions" href=""><strong>{l s='General terms and conditions' mod='gsproductreturn'}</strong></a>
        <br>
        <a id="linkToDataPolicy" href=""><strong>{l s='Personal data policy' mod='gsproductreturn'}</strong></a>
    </div>
</form>




