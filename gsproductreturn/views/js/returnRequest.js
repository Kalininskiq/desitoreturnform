/**
 * 2007-2020 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License (AFL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/afl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to http://www.prestashop.com for more information.
 *
 *  @author    PrestaShop SA <contact@prestashop.com>
 *  @copyright 2007-2020 PrestaShop SA
 *  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
 *  International Registered Trademark & Property of PrestaShop SA
 */



$(document).ready(function (){
    $("#product_description").hide();
    // HIDE/SHOW product description text area
    $('input[name=returnType]').change(
        function(){
            if($('#returnCash').is(':checked') || $('#coupon').is(':checked'))
            {
                $("#product_description").fadeOut("slow");
                $("#productDescription").val(" ");
            }
            if ($('#returnProduct').is(':checked')) {
                $("#product_description").fadeIn("slow");
            }
        });
    var urlToDataPolicy = UrlToDataPolicy;
    var urlToGeneralTerms = UrlToGeneralTerms;

    $("#linktoTermsAndContidions").attr("href", urlToGeneralTerms)
    $("#linkToDataPolicy").attr("href", urlToDataPolicy)
  $(document).on('click','button#ReturnProductButton',function (e){
      e.preventDefault();
      $("span[id*='error']").each(function (i, el) {
          var id;
          id = ($(this).attr('id'));
          $('#'+id).empty();
          $('#'+id).removeAttr('style');
      });

      $("#success").empty();
      $("#success").hide();

      var userName = $('input[name=user_name]#userName').val();
      var userEmail = $('input[name=email]#userEmail').val();
      var productDescription = $('textarea#productDescription').val();
      var returnType  = $("input[name=returnType]:checked").val();
      var returnReason = $("input[name=returnReason]:checked").val();
      var userPhoneNumber = $('input[name=userPhone]#userPhone').val();
      var personalDataPolicy = $('input[name=personalDataPolicy]#personalDataPolicy').is(':checked') ? true : false;
      var termsAndConditions = $('input[name=termsAndConditions]#termsAndConditions').is(':checked') ? true : false;



      var request_data = {
          user_name : userName,
          user_email : userEmail,
          return_type : returnType,
          return_reason : returnReason,
          product_description : productDescription,
          user_phone: userPhoneNumber,
          terms : termsAndConditions,
          data_policy : personalDataPolicy,
      }
      $.ajax({
          type:"POST",
          url:UrlToController,
          data:request_data,
          success: function (data,response,xhr) {

              var result  = JSON.parse(data);
              if(result.errors !== undefined)
              {
                  $.each(result.errors,function (key,value){
                      if(key == "email"){
                          $("#email-error").css('color','red')
                          $("#email-error").append('<p>'+ value + '</p>');
                      }
                      if (key == "return-type"){
                          $("#return-type-error").css('color','red')
                          $("#return-type-error").append('<p>'+ value + '</p>');
                      }
                      if (key == "username"){
                          $("#username-error").css('color','red')
                          $("#username-error").append('<p>'+ value + '</p>');
                      }
                      if (key == "phone"){
                          $("#phone-error").css('color','red')
                          $("#phone-error").append('<p>'+ value + '</p>');
                      }
                      if(key == "description")
                      {
                          $("#product-description-error").css('color','red')
                          $("#product-description-error").append('<p>'+ value + '</p>');
                      }
                      if(key == "checkboxes")
                      {
                          $("#checkbox-error").css('color','red')
                          $("#checkbox-error").append('<p>'+ value + '</p>');
                      }
                  });

              }

                if(result.success !== undefined)
                {
                    $.each(result.success,function (key,value){
                        $("#success").addClass("alert alert-success");
                        $("#success").append('<p>'+ value + '</p>')
                    });
                }
                if(result.success !== undefined)
                {
                    $("#success").show().delay(4000).hide(1000);
                }

              $("span[id*='error']").each(function (i, el) {
                  var id;
                  id = ($(this).attr('id'));
                  $('#'+id).show().delay(5000).hide(1);
              });

          }
      });
  })
})
